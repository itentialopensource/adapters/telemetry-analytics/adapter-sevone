## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for SevOne. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for SevOne.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the SevOne. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(alertId, uriOptions, callback)</td>
    <td style="padding:15px">get alerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsFiltered(filterObj, uriOptions, callback)</td>
    <td style="padding:15px">get alerts based on filter</td>
    <td style="padding:15px">{base_path}/{version}/alerts/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsForDevice(deviceId, deviceName, ipAddr, uriOptions, callback)</td>
    <td style="padding:15px">get alerts for the device</td>
    <td style="padding:15px">{base_path}/{version}/alerts/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsForMapConnection(mapId, connectionId, uriOptions, callback)</td>
    <td style="padding:15px">get alerts for the map connection</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connection/{pathv2}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsForMapNode(mapId, nodeId, uriOptions, callback)</td>
    <td style="padding:15px">get alerts for the map node</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/node/{pathv2}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlert(alertObj, callback)</td>
    <td style="padding:15px">create alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlert(alertId, alertObj, callback)</td>
    <td style="padding:15px">update alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert(alertId, username, callback)</td>
    <td style="padding:15px">assign an alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/assign/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ignoreAlert(alertId, message, ignoreUntil, callback)</td>
    <td style="padding:15px">ignore an alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/ignore/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAlert(alertId, message, callback)</td>
    <td style="padding:15px">clear an alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlert(alertId, callback)</td>
    <td style="padding:15px">delete alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(deviceId, deviceName, ipAddr, uriOptions, callback)</td>
    <td style="padding:15px">get devices</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSevDevicesFiltered(filterObj, uriOptions, callback)</td>
    <td style="padding:15px">get devices filtered</td>
    <td style="padding:15px">{base_path}/{version}/devices/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(deviceObj, callback)</td>
    <td style="padding:15px">create device</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(deviceId, deviceObj, callback)</td>
    <td style="padding:15px">update device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(deviceId, callback)</td>
    <td style="padding:15px">delete device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroups(groupId, uriOptions, callback)</td>
    <td style="padding:15px">get device groups</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroup(groupObj, callback)</td>
    <td style="padding:15px">create device group</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceToGroup(deviceId, groupId, callback)</td>
    <td style="padding:15px">add device to device group</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroup(groupId, groupObj, callback)</td>
    <td style="padding:15px">update device group</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroup(groupId, callback)</td>
    <td style="padding:15px">delete device group</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceFromGroup(deviceId, groupId, callback)</td>
    <td style="padding:15px">remove device from device group</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceComponents(deviceId, deviceName, componentId, componentName, uriOptions, callback)</td>
    <td style="padding:15px">get device component</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceComponentsFiltered(filterObj, uriOptions, callback)</td>
    <td style="padding:15px">get device components filtered</td>
    <td style="padding:15px">{base_path}/{version}/devices/objects/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceComponent(deviceId, componentObj, callback)</td>
    <td style="padding:15px">create device component</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceComponent(deviceId, componentId, componentObj, callback)</td>
    <td style="padding:15px">update device component</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceComponent(deviceId, componentId, callback)</td>
    <td style="padding:15px">delete device component</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponentGroups(groupId, uriOptions, callback)</td>
    <td style="padding:15px">get device component groups</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createComponentGroup(groupObj, callback)</td>
    <td style="padding:15px">create device component group</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceComponentToGroup(deviceId, componentId, groupId, callback)</td>
    <td style="padding:15px">add device to device component group</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateComponentGroup(groupId, groupObj, callback)</td>
    <td style="padding:15px">update device component group</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComponentGroup(groupId, callback)</td>
    <td style="padding:15px">delete device component group</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceComponentFromGroup(deviceId, componentId, groupId, callback)</td>
    <td style="padding:15px">remove device from device component group</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicators(deviceId, deviceName, componentId, componentName, indicatorId, uriOptions, callback)</td>
    <td style="padding:15px">get indicators</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/indicators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorData(deviceId, deviceName, componentId, componentName, indicatorId, startTime, endTime, callback)</td>
    <td style="padding:15px">get indicator data</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/indicators/{pathv3}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIndicatorData(indicatorObj, callback)</td>
    <td style="padding:15px">create indicator data</td>
    <td style="padding:15px">{base_path}/{version}/device-indicators/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaps(mapId, uriOptions, callback)</td>
    <td style="padding:15px">get maps</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMap(mapObj, callback)</td>
    <td style="padding:15px">create map</td>
    <td style="padding:15px">{base_path}/{version}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMap(mapId, mapObj, callback)</td>
    <td style="padding:15px">update map</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMap(mapId, callback)</td>
    <td style="padding:15px">delete map</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapConnections(mapId, uriOptions, callback)</td>
    <td style="padding:15px">get map connections</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapConnection(mapId, connectionObj, callback)</td>
    <td style="padding:15px">create map connection</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMapConnection(mapId, connectionId, connectionObj, callback)</td>
    <td style="padding:15px">update map connection</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMapConnection(mapId, connectionId, callback)</td>
    <td style="padding:15px">delete map connection</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapNodes(mapId, uriOptions, callback)</td>
    <td style="padding:15px">get map nodes</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapNode(mapId, nodeObj, callback)</td>
    <td style="padding:15px">create map node</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMapNode(mapId, nodeId, nodeObj, callback)</td>
    <td style="padding:15px">update map node</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMapNode(mapId, nodeId, callback)</td>
    <td style="padding:15px">delete map node</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentResources(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectAttachment(id, body, callback)</td>
    <td style="padding:15px">createObjectAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypes(page, size, includeMembers, callback)</td>
    <td style="padding:15px">getDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceType(body, callback)</td>
    <td style="padding:15px">createDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypeForDeviceById(deviceId, callback)</td>
    <td style="padding:15px">getDeviceTypeForDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes/members/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypeById(id, callback)</td>
    <td style="padding:15px">getDeviceTypeById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTypeById(id, callback)</td>
    <td style="padding:15px">deleteDeviceTypeById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMemberByIdToType(id, deviceId, callback)</td>
    <td style="padding:15px">addMemberByIdToType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTypeMemberById(id, deviceId, callback)</td>
    <td style="padding:15px">deleteDeviceTypeMemberById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetypes/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectById(deviceId, id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusMapAttachmentResources(id, callback)</td>
    <td style="padding:15px">getStatusMapAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/statusmap/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatusMapAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateStatusMapAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/statusmap/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStatusMapAttachment(id, body, callback)</td>
    <td style="padding:15px">createStatusMapAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/statusmap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiKeys(callback)</td>
    <td style="padding:15px">getApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiKey(body, callback)</td>
    <td style="padding:15px">createApiKey</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeys(callback)</td>
    <td style="padding:15px">deleteApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiKey(apiKey, body, callback)</td>
    <td style="padding:15px">updateApiKey</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKey(apiKey, callback)</td>
    <td style="padding:15px">deleteApiKey</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiKeysForUser(id, callback)</td>
    <td style="padding:15px">getApiKeysForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiKeyForUser(id, body, callback)</td>
    <td style="padding:15px">createApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeysForUser(id, callback)</td>
    <td style="padding:15px">deleteApiKeysForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiKeyForUser(id, apiKey, body, callback)</td>
    <td style="padding:15px">updateApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/api-keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeyForUser(apiKey, id, callback)</td>
    <td style="padding:15px">deleteApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/api-keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesInDiscovery(page, size, includeMembers, callback)</td>
    <td style="padding:15px">getDevicesInDiscovery</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterDevicesInDiscovery(page, size, includeMembers, includeObjects, includeIndicators, body, callback)</td>
    <td style="padding:15px">filterDevicesInDiscovery</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/discovery/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceStatusById(id, includeMembers, callback)</td>
    <td style="padding:15px">getDeviceStatusById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevicePriority(id, body, callback)</td>
    <td style="padding:15px">updateDevicePriority</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentResources(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlowFalconAttachment(id, body, callback)</td>
    <td style="padding:15px">createFlowFalconAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/flow-falcon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentResources(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateTopNAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateTopNAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopNAttachment(id, body, callback)</td>
    <td style="padding:15px">createTopNAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPeersUsingGET(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPeersUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterSettings(filter, callback)</td>
    <td style="padding:15px">getClusterSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/clusterSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentPeerUsingGET(callback)</td>
    <td style="padding:15px">getCurrentPeerUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncorporateModeUsingGET(callback)</td>
    <td style="padding:15px">getIncorporateModeUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/incorporateMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIncorporateModeUsingPATCH(body, callback)</td>
    <td style="padding:15px">editIncorporateModeUsingPATCH</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/incorporateMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPeerUsingGET(id, callback)</td>
    <td style="padding:15px">getPeerUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettings(id, filter, callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/peers/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportAttachment(id, callback)</td>
    <td style="padding:15px">getReportAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportAttachmentById(id, body, callback)</td>
    <td style="padding:15px">updateReportAttachmentById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportAttachment(id, callback)</td>
    <td style="padding:15px">deleteReportAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportAttachmentEndpoints(type = 'Device', callback)</td>
    <td style="padding:15px">getAllReportAttachmentEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportAttachments(id, page, size, callback)</td>
    <td style="padding:15px">getAllReportAttachments</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupsRules(page, size, callback)</td>
    <td style="padding:15px">getObjectGroupsRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectGroupRule(body, callback)</td>
    <td style="padding:15px">createObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupRule(id, callback)</td>
    <td style="padding:15px">getObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupRule(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectGroupRule(id, callback)</td>
    <td style="padding:15px">deleteObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupRules(id, page, size, callback)</td>
    <td style="padding:15px">getObjectGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyObjectGroupRules(id, callback)</td>
    <td style="padding:15px">applyObjectGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/{pathv1}/rules/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentUser(callback)</td>
    <td style="padding:15px">getCurrentUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupAttachmentResources(id, callback)</td>
    <td style="padding:15px">getObjectGroupAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/object-groups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/object-groups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectGroupAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectGroupAttachment(id, body, callback)</td>
    <td style="padding:15px">createObjectGroupAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/object-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateStatusMapById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateStatusMapById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getDeviceAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachment(id, callback)</td>
    <td style="padding:15px">getDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachment(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devices/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceAttachment(id, body, callback)</td>
    <td style="padding:15px">createDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsAttachment(id, callback)</td>
    <td style="padding:15px">getDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devicegroups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupsAttachment(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devicegroups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getDeviceGroupsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devicegroups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupsAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/devicegroups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroupsAttachment(id, body, callback)</td>
    <td style="padding:15px">createDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataAttachmentResources(id, callback)</td>
    <td style="padding:15px">getMetadataAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/metadata/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateMetadataAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/metadata/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateMetadataAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMetadataAttachment(id, body, callback)</td>
    <td style="padding:15px">createMetadataAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentResources(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPerformanceMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">createPerformanceMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/performance-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachment(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/visualization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/group-metrics/{pathv1}/visualization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">createGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/group-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessions(token, callback)</td>
    <td style="padding:15px">getActiveSessions</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/active-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keepAlive(callback)</td>
    <td style="padding:15px">keepAlive</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signIn(nmsLogin, body, callback)</td>
    <td style="padding:15px">signIn</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/signin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOut(callback)</td>
    <td style="padding:15px">signOut</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/signout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOutOthers(callback)</td>
    <td style="padding:15px">signOutOthers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/signout-others?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOutUser(userId, callback)</td>
    <td style="padding:15px">signOutUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authentication/signout/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateDeviceGroupById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateDeviceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTagsById(id, callback)</td>
    <td style="padding:15px">getDeviceTagsById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicetags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getAlertAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentAggregation(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentAggregation(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentResource(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentResource</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentResource(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentResource</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateAlertAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertAttachment(id, body, callback)</td>
    <td style="padding:15px">createAlertAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPlugins(page, size, name, objectName, callback)</td>
    <td style="padding:15px">getAllPlugins</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicePluginInfoSchema(callback)</td>
    <td style="padding:15px">getDevicePluginInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/device/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorExtendedInfoSchema(pluginId, callback)</td>
    <td style="padding:15px">getIndicatorExtendedInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicator/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPluginIndicatorTypes(page, size, includeExtendedInfo, callback)</td>
    <td style="padding:15px">getAllPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicatortypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPluginIndicatorType(body, callback)</td>
    <td style="padding:15px">createPluginIndicatorType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicatortypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPluginIndicatorTypes(page, size, includeExtendedInfo, body, callback)</td>
    <td style="padding:15px">filterPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicatortypes/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemaForAllPluginIndicatorTypes(pluginId, callback)</td>
    <td style="padding:15px">getSchemaForAllPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicatortypes/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePluginIndicatorType(id, body, callback)</td>
    <td style="padding:15px">updatePluginIndicatorType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/indicatortypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectExtendedInfoSchema(pluginId, callback)</td>
    <td style="padding:15px">getObjectExtendedInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/object/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPluginObjectTypes(page, size, includeExtendedInfo, callback)</td>
    <td style="padding:15px">getAllPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/objecttypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPluginObjectType(body, callback)</td>
    <td style="padding:15px">createPluginObjectType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/objecttypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPluginObjectTypes(page, size, includeExtendedInfo, body, callback)</td>
    <td style="padding:15px">filterPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/objecttypes/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemaForAllPluginObjectTypes(pluginId, callback)</td>
    <td style="padding:15px">getSchemaForAllPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/objecttypes/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePluginObjectType(id, body, callback)</td>
    <td style="padding:15px">updatePluginObjectType</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/plugins/objecttypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertForced(body, callback)</td>
    <td style="padding:15px">createAlertForced</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/alerts/force?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconDeviceAlerts(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getFlowFalconDeviceAlerts</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/alerts/netflow-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaxSeverityAlertForObjects(body, callback)</td>
    <td style="padding:15px">getMaxSeverityAlertForObjects</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/alerts/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlert(id, body, callback)</td>
    <td style="padding:15px">patchAlert</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimezonesByCountries(callback)</td>
    <td style="padding:15px">getTimezonesByCountries</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/countries/timezones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicies(type = 'other', page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPolicies(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterPolicies</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFolders(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPolicyFolders</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyFolder(body, callback)</td>
    <td style="padding:15px">createPolicyFolder</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyFolder(id, body, callback)</td>
    <td style="padding:15px">updatePolicyFolder</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyFolderById(id, callback)</td>
    <td style="padding:15px">deletePolicyFolderById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(id, callback)</td>
    <td style="padding:15px">getPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(id, body, callback)</td>
    <td style="padding:15px">updatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyById(id, callback)</td>
    <td style="padding:15px">deletePolicyById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActionsUsingGET(policyId, callback)</td>
    <td style="padding:15px">getActionsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyAction(policyId, body, callback)</td>
    <td style="padding:15px">updatePolicyAction</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyActionById(policyId, callback)</td>
    <td style="padding:15px">deletePolicyActionById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findAllUsingGET(page, size, includeCount, sortBy, fields, policyId, callback)</td>
    <td style="padding:15px">findAllUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyCondition(policyId, body, callback)</td>
    <td style="padding:15px">createPolicyCondition</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyConditionById(policyId, conditionId, callback)</td>
    <td style="padding:15px">getPolicyConditionById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyCondition(policyId, conditionId, body, callback)</td>
    <td style="padding:15px">updatePolicyCondition</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyConditionById(policyId, conditionId, callback)</td>
    <td style="padding:15px">deletePolicyConditionById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReports(page, size, userId, name, callback)</td>
    <td style="padding:15px">getAllReports</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReport(body, callback)</td>
    <td style="padding:15px">createReport</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportFolders(page, size, callback)</td>
    <td style="padding:15px">getAllReportFolders</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReportFolder(body, callback)</td>
    <td style="padding:15px">createReportFolder</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportFolderById(id, body, callback)</td>
    <td style="padding:15px">updateReportFolderById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportFolderById(id, callback)</td>
    <td style="padding:15px">deleteReportFolderById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReport(id, callback)</td>
    <td style="padding:15px">getReport</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportById(id, body, callback)</td>
    <td style="padding:15px">updateReportById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportById(id, callback)</td>
    <td style="padding:15px">deleteReportById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentAggregation(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentAggregation(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/attachments/telephony/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTelephonyAttachment(id, body, callback)</td>
    <td style="padding:15px">createTelephonyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/reports/{pathv1}/attachments/telephony?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDynamicPluginManager(body, callback)</td>
    <td style="padding:15px">registerDynamicPluginManager</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/pluginmanager/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDynamicPlugin(id, body, callback)</td>
    <td style="padding:15px">registerDynamicPlugin</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/pluginmanager/{pathv1}/plugin/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectGroupById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectGroupById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateDeviceById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapImages(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getMapImages</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mapimages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapImageById(id, callback)</td>
    <td style="padding:15px">getMapImageById</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mapimages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoggingLevel(body, callback)</td>
    <td style="padding:15px">postLoggingLevel</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/application/logger?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicMetrics(filter, callback)</td>
    <td style="padding:15px">getPublicMetrics</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/application/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">getVersion</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/application/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsRules(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceGroupsRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroupRule(body, callback)</td>
    <td style="padding:15px">createDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupRule(id, callback)</td>
    <td style="padding:15px">getDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupRule(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupRule(id, callback)</td>
    <td style="padding:15px">deleteDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupRules(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/devicegroups/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
