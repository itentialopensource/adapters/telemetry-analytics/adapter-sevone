# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the SevOne System. The API that was used to build the adapter for SevOne is usually available in the report directory of this adapter. The adapter utilizes the SevOne API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The SevOne adapter from Itential is used to integrate the Itential Automation Platform (IAP) with IBM SevOne Network Performance Management to monitor network and infrastructure to maintain the reliability and quality of the services. With this adapter you have the ability to perform operations such as:

- Retrieve, Create, or Clear Alerts
- Retrieve, Create, or Update Policies
- Get, Create, Update, or Delete Device
- Get, Create, Update, or Delete Device Groups

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
