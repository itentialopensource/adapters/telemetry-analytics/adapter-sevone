/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-sevone',
      type: 'SevOne',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const SevOne = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] SevOne Adapter Test', () => {
  describe('SevOne Class Tests', () => {
    const a = new SevOne(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('sevone'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('sevone'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('SevOne', pronghornDotJson.export);
          assert.equal('SevOne', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-sevone', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('sevone'));
          assert.equal('SevOne', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-sevone', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-sevone', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getDevices - errors', () => {
      it('should have a getDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesById - errors', () => {
      it('should have a getDevicesById function', (done) => {
        assert.equal(true, typeof a.getDevicesById === 'function');
        done();
      });
    });

    describe('#getDevicesByName - errors', () => {
      it('should have a getDevicesByName function', (done) => {
        assert.equal(true, typeof a.getDevicesByName === 'function');
        done();
      });
    });

    describe('#getDevicesByIp - errors', () => {
      it('should have a getDevicesByIp function', (done) => {
        assert.equal(true, typeof a.getDevicesByIp === 'function');
        done();
      });
    });

    describe('#getSevDevicesFiltered - errors', () => {
      it('should have a getSevDevicesFiltered function', (done) => {
        assert.equal(true, typeof a.getSevDevicesFiltered === 'function');
        done();
      });
    });

    describe('#getDevicesForNameorIP - errors', () => {
      it('should have a getDevicesForNameorIP function', (done) => {
        assert.equal(true, typeof a.getDevicesForNameorIP === 'function');
        done();
      });
    });

    const deviceId = 1234;
    describe('#createDevice - errors', () => {
      it('should have a createDevice function', (done) => {
        assert.equal(true, typeof a.createDevice === 'function');
        done();
      });
      it('should error on create a device - no device', (done) => {
        try {
          a.createDevice(null, (data, error) => {
            try {
              const displayE = 'deviceObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device - no name', (done) => {
        try {
          const device = { ipAddress: '10.10.10.1' };
          a.createDevice(device, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevice - errors', () => {
      it('should have a updateDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a device - no id', (done) => {
        try {
          a.updateDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no changes', (done) => {
        try {
          a.updateDevice(deviceId, null, (data, error) => {
            try {
              const displayE = 'deviceObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no name', (done) => {
        try {
          const device = { ipAddress: '10.10.10.1' };
          a.updateDevice(deviceId, device, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevice - errors', () => {
      it('should have a deleteDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a device - no id', (done) => {
        try {
          a.deleteDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroups - errors', () => {
      it('should have a getDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDeviceGroupsById - errors', () => {
      it('should have a getDeviceGroupsById function', (done) => {
        assert.equal(true, typeof a.getDeviceGroupsById === 'function');
        done();
      });
    });

    const deviceGrpId = 1;
    describe('#createDeviceGroup - errors', () => {
      it('should have a createDeviceGroup function', (done) => {
        assert.equal(true, typeof a.createDeviceGroup === 'function');
        done();
      });
      it('should error on create a device group - no device group', (done) => {
        try {
          a.createDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'groupObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device group - no name', (done) => {
        try {
          const deviceGrp = { parentId: 1 };
          a.createDeviceGroup(deviceGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device group - no parent id', (done) => {
        try {
          const deviceGrp = { name: 'blah' };
          a.createDeviceGroup(deviceGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeviceToGroup - errors', () => {
      it('should have a addDeviceToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addDeviceToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on add a device to group - no group', (done) => {
        try {
          a.addDeviceToGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addDeviceToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on add a device to group - no device', (done) => {
        try {
          a.addDeviceToGroup(null, deviceGrpId, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addDeviceToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroup - errors', () => {
      it('should have a updateDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a device group - no id', (done) => {
        try {
          a.updateDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device group - no changes', (done) => {
        try {
          a.updateDeviceGroup(deviceGrpId, null, (data, error) => {
            try {
              const displayE = 'groupObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device group - no name', (done) => {
        try {
          const deviceGrp = { parentId: 1 };
          a.updateDeviceGroup(deviceGrpId, deviceGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device group - no parent id', (done) => {
        try {
          const deviceGrp = { name: 'blah' };
          a.updateDeviceGroup(deviceGrpId, deviceGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup - errors', () => {
      it('should have a deleteDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a device group - no id', (done) => {
        try {
          a.deleteDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeDeviceFromGroup - errors', () => {
      it('should have a removeDeviceFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeDeviceFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on remove a device from group - no group', (done) => {
        try {
          a.removeDeviceFromGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-removeDeviceFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on remove a device from group - no device', (done) => {
        try {
          a.removeDeviceFromGroup(null, deviceGrpId, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-removeDeviceFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponents - errors', () => {
      it('should have a getDeviceComponents function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceComponents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get a device component - need more than component name', (done) => {
        try {
          a.getDeviceComponents(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentsById - errors', () => {
      it('should have a getDeviceComponentsById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceComponentsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDeviceComponentsByName - errors', () => {
      it('should have a getDeviceComponentsByName function', (done) => {
        assert.equal(true, typeof a.getDeviceComponentsByName === 'function');
        done();
      });
      it('should error on get a device component - need more than component name', (done) => {
        try {
          a.getDeviceComponentsByName(null, 'componentName', (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentsFiltered - errors', () => {
      it('should have a getDeviceComponentsFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceComponentsFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDeviceComponentId - errors', () => {
      it('should have a getDeviceComponentId function', (done) => {
        assert.equal(true, typeof a.getDeviceComponentId === 'function');
        done();
      });
      it('should error on get a device component id - need device', (done) => {
        try {
          a.getDeviceComponentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get a device component id - need component name', (done) => {
        try {
          a.getDeviceComponentId(null, 'deviceName', null, null, (data, error) => {
            try {
              const displayE = 'componentName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceCompId = 1234;

    describe('#createDeviceComponent - errors', () => {
      it('should have a createDeviceComponent function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on create a device component - no device', (done) => {
        try {
          a.createDeviceComponent(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on create a device component - no dev component', (done) => {
        try {
          a.createDeviceComponent(deviceId, null, (data, error) => {
            try {
              const displayE = 'componentObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device component - no name', (done) => {
        try {
          const deviceComp = {
            description: 'blah', pluginId: 1, pluginObjectTypeId: 1
          };
          a.createDeviceComponent(deviceId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device component - no description', (done) => {
        try {
          const deviceComp = {
            name: 'blah', pluginId: 1, pluginObjectTypeId: 1
          };
          a.createDeviceComponent(deviceId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'description\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device component - no plugin id', (done) => {
        try {
          const deviceComp = {
            name: 'blah', description: 'blah', pluginObjectTypeId: 1
          };
          a.createDeviceComponent(deviceId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'pluginId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a device component - no plugin object type id', (done) => {
        try {
          const deviceComp = {
            name: 'blah', description: 'blah', pluginId: 1
          };
          a.createDeviceComponent(deviceId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'pluginObjectTypeId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceComponent - errors', () => {
      it('should have a updateDeviceComponent function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a device component - no device id', (done) => {
        try {
          a.updateDeviceComponent(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on update a device component - no component id', (done) => {
        try {
          a.updateDeviceComponent(deviceId, null, null, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no changes', (done) => {
        try {
          a.updateDeviceComponent(deviceId, deviceCompId, null, (data, error) => {
            try {
              const displayE = 'componentObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no name', (done) => {
        try {
          const deviceComp = {
            description: 'change blah', pluginId: 1, pluginObjectTypeId: 1
          };
          a.updateDeviceComponent(deviceId, deviceCompId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no desciption', (done) => {
        try {
          const deviceComp = {
            name: 'changeblah', pluginId: 1, pluginObjectTypeId: 1
          };
          a.updateDeviceComponent(deviceId, deviceCompId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'description\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a device - no plugin id', (done) => {
        try {
          const deviceComp = {
            name: 'changeblah', description: 'blah', pluginObjectTypeId: 1
          };
          a.updateDeviceComponent(deviceId, deviceCompId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'pluginId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on update a device - no plugin object type id', (done) => {
        try {
          const deviceComp = {
            name: 'changeblah', description: 'blah', pluginId: 1
          };
          a.updateDeviceComponent(deviceId, deviceCompId, deviceComp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'pluginObjectTypeId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceComponent - errors', () => {
      it('should have a deleteDeviceComponent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a device component - no device id', (done) => {
        try {
          a.deleteDeviceComponent(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on delete a device component - no component id', (done) => {
        try {
          a.deleteDeviceComponent(deviceId, null, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentGroups - errors', () => {
      it('should have a getComponentGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getComponentGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getComponentGroupsById - errors', () => {
      it('should have a getComponentGroupsById function', (done) => {
        assert.equal(true, typeof a.getComponentGroupsById === 'function');
        done();
      });
    });

    const componentGrpId = 1;
    describe('#createComponentGroup - errors', () => {
      it('should have a createComponentGroup function', (done) => {
        assert.equal(true, typeof a.createComponentGroup === 'function');
        done();
      });
      it('error on create a component group - no component group', (done) => {
        try {
          a.createComponentGroup(null, (data, error) => {
            try {
              const displayE = 'groupObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a component group - no name', (done) => {
        try {
          const componentGrp = { parentId: 1 };
          a.createComponentGroup(componentGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a component group - no parent id', (done) => {
        try {
          const componentGrp = { name: 'blah' };
          a.createComponentGroup(componentGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeviceComponentToGroup - errors', () => {
      it('should have a addDeviceComponentToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addDeviceComponentToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on add a device component to group - no group', (done) => {
        try {
          a.addDeviceComponentToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addDeviceComponentToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on add a device component to group - no device', (done) => {
        try {
          a.addDeviceComponentToGroup(null, null, componentGrpId, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addDeviceComponentToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on add a device component to group - no component', (done) => {
        try {
          a.addDeviceComponentToGroup(deviceId, null, componentGrpId, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addDeviceComponentToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateComponentGroup - errors', () => {
      it('should have a updateComponentGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateComponentGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a component group - no id', (done) => {
        try {
          a.updateComponentGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a component group - no changes', (done) => {
        try {
          a.updateComponentGroup(componentGrpId, null, (data, error) => {
            try {
              const displayE = 'groupObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a component group - no name', (done) => {
        try {
          const componentGrp = { parentId: 1 };
          a.updateComponentGroup(componentGrpId, componentGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a component group - no parent id', (done) => {
        try {
          const componentGrp = { name: 'blah' };
          a.updateComponentGroup(componentGrpId, componentGrp, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteComponentGroup - errors', () => {
      it('should have a deleteComponentGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteComponentGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a component group - no id', (done) => {
        try {
          a.deleteComponentGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeDeviceComponentFromGroup - errors', () => {
      it('should have a removeDeviceComponentFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeDeviceComponentFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on remove a device component from group - no group', (done) => {
        try {
          a.removeDeviceComponentFromGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-removeDeviceComponentFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on remove a device component from grp - no device', (done) => {
        try {
          a.removeDeviceComponentFromGroup(null, null, componentGrpId, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-removeDeviceComponentFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on remove a dev component from grp - no component', (done) => {
        try {
          a.removeDeviceComponentFromGroup(deviceId, null, componentGrpId, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-removeDeviceComponentFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicators - errors', () => {
      it('should have a getIndicators function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicators === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator - no device id', (done) => {
        try {
          a.getIndicators(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator - no component id', (done) => {
        try {
          a.getIndicators(deviceId, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorsById - errors', () => {
      it('should have a getIndicatorsById function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator by id - no device id', (done) => {
        try {
          a.getIndicatorsById(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator by id - no component id', (done) => {
        try {
          a.getIndicatorsById(deviceId, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorsByName - errors', () => {
      it('should have a getIndicatorsByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorsByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator by name - no device name', (done) => {
        try {
          a.getIndicatorsByName(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator by name - no component name', (done) => {
        try {
          a.getIndicatorsByName('deviceName', null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicators', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorData - errors', () => {
      it('should have a getIndicatorData function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator data - no device id', (done) => {
        try {
          a.getIndicatorData(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data - no component id', (done) => {
        try {
          a.getIndicatorData(deviceId, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data - no indicator id', (done) => {
        try {
          a.getIndicatorData(deviceId, null, deviceCompId, null, null, null, null, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorDataWithRangeById - errors', () => {
      it('should have a getIndicatorDataWithRangeById function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorDataWithRangeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator data by id - no device id', (done) => {
        try {
          a.getIndicatorDataWithRangeById(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data by id - no component id', (done) => {
        try {
          a.getIndicatorDataWithRangeById(deviceId, null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data by id - no indicator id', (done) => {
        try {
          a.getIndicatorDataWithRangeById(deviceId, deviceCompId, null, null, null, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorDataWithRangeByName - errors', () => {
      it('should have a getIndicatorDataWithRangeByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorDataWithRangeByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator data by name - no device name', (done) => {
        try {
          a.getIndicatorDataWithRangeByName(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data by name - no component name', (done) => {
        try {
          a.getIndicatorDataWithRangeByName('deviceName', null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data by name - no indicator id', (done) => {
        try {
          a.getIndicatorDataWithRangeByName('deviceName', 'componentName', null, null, null, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorDataForDayById - errors', () => {
      it('should have a getIndicatorDataForDayById function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorDataForDayById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator data for day by id - no device id', (done) => {
        try {
          a.getIndicatorDataForDayById(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data for day by id - no component id', (done) => {
        try {
          a.getIndicatorDataForDayById(deviceId, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data for day by id - no indicator id', (done) => {
        try {
          a.getIndicatorDataForDayById(deviceId, deviceCompId, null, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorDataForDayByName - errors', () => {
      it('should have a getIndicatorDataForDayByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorDataForDayByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get indicator data for day by name - no device name', (done) => {
        try {
          a.getIndicatorDataForDayByName(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data for day by name - no component name', (done) => {
        try {
          a.getIndicatorDataForDayByName('deviceName', null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get indicator data for day by name - no indicator id', (done) => {
        try {
          a.getIndicatorDataForDayByName('deviceName', 'componentName', null, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const indicatorId = 0;

    describe('#createIndicatorData - errors', () => {
      it('should have a createIndicatorData function', (done) => {
        try {
          assert.equal(true, typeof a.createIndicatorData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on create indicator data - no indicator data', (done) => {
        try {
          a.createIndicatorData(null, (data, error) => {
            try {
              const displayE = 'indicatorObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a indicator data - no device id', (done) => {
        try {
          const indicator = {
            componentId: deviceCompId,
            indicatorData: [{ indicatorId, value: 1 }]
          };
          a.createIndicatorData(indicator, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on create a indicator data - no component id', (done) => {
        try {
          const indicator = {
            deviceId, indicatorData: [{ indicatorId, value: 1 }]
          };
          a.createIndicatorData(indicator, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on create a indicator data - no indicator id', (done) => {
        try {
          const indicator = {
            deviceId, componentId: deviceCompId, indicatorData: [{ value: 1 }]
          };
          a.createIndicatorData(indicator, (data, error) => {
            try {
              const displayE = 'indicatorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a indicator data - no value', (done) => {
        try {
          const indicator = {
            deviceId,
            componentId: deviceCompId,
            indicatorData: [{ indicatorId }]
          };
          a.createIndicatorData(indicator, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createIndicatorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaps - errors', () => {
      it('should have a getMaps function', (done) => {
        try {
          assert.equal(true, typeof a.getMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getMapsById - errors', () => {
      it('should have a getMapsById function', (done) => {
        assert.equal(true, typeof a.getMapsById === 'function');
        done();
      });
    });

    const mapId = 1;
    describe('#createMap - errors', () => {
      it('should have a createMap function', (done) => {
        assert.equal(true, typeof a.createMap === 'function');
        done();
      });
      it('should error on create a map - no map', (done) => {
        try {
          a.createMap(null, (data, error) => {
            try {
              const displayE = 'mapObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map - no name', (done) => {
        try {
          const mapObj = { imageId: 1 };
          a.createMap(mapObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map - no image id', (done) => {
        try {
          const mapObj = { name: 'blah' };
          a.createMap(mapObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'imageId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMap - errors', () => {
      it('should have a updateMap function', (done) => {
        try {
          assert.equal(true, typeof a.updateMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a map - no id', (done) => {
        try {
          a.updateMap(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map - no changes', (done) => {
        try {
          a.updateMap(mapId, null, (data, error) => {
            try {
              const displayE = 'mapObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map - no name', (done) => {
        try {
          const mapObj = { imageId: 1 };
          a.updateMap(mapId, mapObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map - no image id', (done) => {
        try {
          const mapObj = { name: 'blah' };
          a.updateMap(mapId, mapObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'imageId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMap - errors', () => {
      it('should have a deleteMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a map - no id', (done) => {
        try {
          a.deleteMap(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapConnections - errors', () => {
      it('should have a getMapConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getMapConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get a map connection - no map id', (done) => {
        try {
          a.getMapConnections(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMapConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapConnectionsById - errors', () => {
      it('should have a getMapConnectionsById function', (done) => {
        try {
          assert.equal(true, typeof a.getMapConnectionsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get a map connection by id - no map id', (done) => {
        try {
          a.getMapConnectionsById(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMapConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mapConnId = 1;

    describe('#createMapConnection - errors', () => {
      it('should have a createMapConnection function', (done) => {
        try {
          assert.equal(true, typeof a.createMapConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on create a map connection - no map id', (done) => {
        try {
          a.createMapConnection(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map connection - no connection', (done) => {
        try {
          a.createMapConnection(mapId, null, (data, error) => {
            try {
              const displayE = 'connectionObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map connection - no node A', (done) => {
        try {
          const mapConnObj = { nodeBSide: 1, type: 'Device', elements: [] };
          a.createMapConnection(mapId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'nodeASide\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map connection - no node B', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, type: 'Device', elements: [] };
          a.createMapConnection(mapId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'nodeBSide\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map connection - no type', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, nodeBSide: 1, elements: [] };
          a.createMapConnection(mapId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map connection - no elements', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, nodeBSide: 1, type: 'Device' };
          a.createMapConnection(mapId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'elements\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMapConnection - errors', () => {
      it('should have a updateMapConnection function', (done) => {
        try {
          assert.equal(true, typeof a.updateMapConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a map connection - no map id', (done) => {
        try {
          a.updateMapConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on update a map connection - no map connection id', (done) => {
        try {
          a.updateMapConnection(mapId, null, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map connection - no changes', (done) => {
        try {
          a.updateMapConnection(mapId, mapConnId, null, (data, error) => {
            try {
              const displayE = 'connectionObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map connection - no node A', (done) => {
        try {
          const mapConnObj = { nodeBSide: 1, type: 'Device', elements: [] };
          a.updateMapConnection(mapId, mapConnId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'nodeASide\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map connection - no node B', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, type: 'Device', elements: [] };
          a.updateMapConnection(mapId, mapConnId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'nodeBSide\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map connection - no type', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, nodeBSide: 1, elements: [] };
          a.updateMapConnection(mapId, mapConnId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map connection - no elements', (done) => {
        try {
          const mapConnObj = { nodeASide: 1, nodeBSide: 1, type: 'Device' };
          a.updateMapConnection(mapId, mapConnId, mapConnObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'elements\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMapConnection - errors', () => {
      it('should have a deleteMapConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMapConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a map connection - no map id', (done) => {
        try {
          a.deleteMapConnection(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on delete a map connection - no map connection id', (done) => {
        try {
          a.deleteMapConnection(mapId, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapNodes - errors', () => {
      it('should have a getMapNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getMapNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get a map node - no map id', (done) => {
        try {
          a.getMapNodes(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMapNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapNodesById - errors', () => {
      it('should have a getMapNodesById function', (done) => {
        try {
          assert.equal(true, typeof a.getMapNodesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get a map node by id - no map id', (done) => {
        try {
          a.getMapNodesById(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMapNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mapNodeId = 1;

    describe('#createMapNode - errors', () => {
      it('should have a createMapNode function', (done) => {
        try {
          assert.equal(true, typeof a.createMapNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on create a map node - no map id', (done) => {
        try {
          a.createMapNode(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no connection', (done) => {
        try {
          a.createMapNode(mapId, null, (data, error) => {
            try {
              const displayE = 'nodeObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no name', (done) => {
        try {
          const mapNodeObj = {
            type: 'Device', x: 1, y: 1, elements: []
          };
          a.createMapNode(mapId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no type', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', x: 1, y: 1, elements: []
          };
          a.createMapNode(mapId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no x', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', y: 1, elements: []
          };
          a.createMapNode(mapId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'x\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no y', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', x: 1, elements: []
          };
          a.createMapNode(mapId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'y\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create a map node - no elements', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', x: 1, y: 1
          };
          a.createMapNode(mapId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'elements\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMapNode - errors', () => {
      it('should have a updateMapNode function', (done) => {
        try {
          assert.equal(true, typeof a.updateMapNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update a map node - no map id', (done) => {
        try {
          a.updateMapNode(null, null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no map node id', (done) => {
        try {
          a.updateMapNode(mapId, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no changes', (done) => {
        try {
          a.updateMapNode(mapId, mapNodeId, null, (data, error) => {
            try {
              const displayE = 'nodeObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no name', (done) => {
        try {
          const mapNodeObj = {
            type: 'Device', x: 1, y: 1, elements: []
          };
          a.updateMapNode(mapId, mapNodeId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no type', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', x: 1, y: 1, elements: []
          };
          a.updateMapNode(mapId, mapNodeId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no x', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', y: 1, elements: []
          };
          a.updateMapNode(mapId, mapNodeId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'x\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no y', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', x: 1, elements: []
          };
          a.updateMapNode(mapId, mapNodeId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'y\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update a map node - no elements', (done) => {
        try {
          const mapNodeObj = {
            name: 'blah', type: 'Device', x: 1, y: 1
          };
          a.updateMapNode(mapId, mapNodeId, mapNodeObj, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'elements\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMapNode - errors', () => {
      it('should have a deleteMapNode function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMapNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete a map node - no map id', (done) => {
        try {
          a.deleteMapNode(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on delete a map node - no map node id', (done) => {
        try {
          a.deleteMapNode(mapId, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts - errors', () => {
      it('should have a getAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.getAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getAlertsById - errors', () => {
      it('should have a getAlertsById function', (done) => {
        assert.equal(true, typeof a.getAlertsById === 'function');
        done();
      });
    });

    describe('#getAlertsFiltered - errors', () => {
      it('should have a getAlertsFiltered function', (done) => {
        assert.equal(true, typeof a.getAlertsFiltered === 'function');
        done();
      });
    });

    describe('#getAlertsForDevice - errors', () => {
      it('should have a getAlertsForDevice function', (done) => {
        assert.equal(true, typeof a.getAlertsForDevice === 'function');
        done();
      });
      it('should error on get alerts for device - no device info', (done) => {
        try {
          a.getAlertsForDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceById - errors', () => {
      it('should have a getAlertsForDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for device by id - no id', (done) => {
        try {
          a.getAlertsForDeviceById(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceByName - errors', () => {
      it('should have a getAlertsForDeviceByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for device by name - no name', (done) => {
        try {
          a.getAlertsForDeviceByName(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceByIp - errors', () => {
      it('should have a getAlertsForDeviceByIp function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceByIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for device by ip - no ip', (done) => {
        try {
          a.getAlertsForDeviceByIp(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDevice - errors', () => {
      it('should have a getAlertCountsForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for device - no id', (done) => {
        try {
          a.getAlertCountsForDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceById - errors', () => {
      it('should have a getAlertCountsForDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for device by id - no id', (done) => {
        try {
          a.getAlertCountsForDeviceById(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceByName - errors', () => {
      it('should have a getAlertCountsForDeviceByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for device by name - no name', (done) => {
        try {
          a.getAlertCountsForDeviceByName(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceByIp - errors', () => {
      it('should have a getAlertCountsForDeviceByIp function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceByIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for device by ip - no ip', (done) => {
        try {
          a.getAlertCountsForDeviceByIp(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRange - errors', () => {
      it('should have a getAlertsForDeviceWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device - no id', (done) => {
        try {
          a.getAlertsForDeviceWithRange(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeById - errors', () => {
      it('should have a getAlertsForDeviceWithRangeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceWithRangeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device by id - no id', (done) => {
        try {
          a.getAlertsForDeviceWithRangeById(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeByName - errors', () => {
      it('should have a getAlertsForDeviceWithRangeByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceWithRangeByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device by name - no name', (done) => {
        try {
          a.getAlertsForDeviceWithRangeByName(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeByIp - errors', () => {
      it('should have a getAlertsForDeviceWithRangeByIp function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceWithRangeByIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device by ip - no ip', (done) => {
        try {
          a.getAlertsForDeviceWithRangeByIp(null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayById - errors', () => {
      it('should have a getAlertsForDeviceForDayById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceForDayById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for device by id - no id', (done) => {
        try {
          a.getAlertsForDeviceForDayById(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayByName - errors', () => {
      it('should have a getAlertsForDeviceForDayByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceForDayByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for device by name - no name', (done) => {
        try {
          a.getAlertsForDeviceForDayByName(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayByIp - errors', () => {
      it('should have a getAlertsForDeviceForDayByIp function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceForDayByIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for device by ip - no ip', (done) => {
        try {
          a.getAlertsForDeviceForDayByIp(null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroup - errors', () => {
      it('should have a getAlertsForDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for device group - no id', (done) => {
        try {
          a.getAlertsForDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupById - errors', () => {
      it('should have a getAlertsForDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for device group by id - no id', (done) => {
        try {
          a.getAlertsForDeviceGroupById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceGroup - errors', () => {
      it('should have a getAlertCountsForDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for device group - no id', (done) => {
        try {
          a.getAlertCountsForDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceGroupById - errors', () => {
      it('should have a getAlertCountsForDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for device group by id - no id', (done) => {
        try {
          a.getAlertCountsForDeviceGroupById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupWithRange - errors', () => {
      it('should have a getAlertsForDeviceGroupWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceGroupWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device group - no id', (done) => {
        try {
          a.getAlertsForDeviceGroupWithRange(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupWithRangeById - errors', () => {
      it('should have a getAlertsForDeviceGroupWithRangeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceGroupWithRangeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for device group by id - no id', (done) => {
        try {
          a.getAlertsForDeviceGroupWithRangeById(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupForDayById - errors', () => {
      it('should have a getAlertsForDeviceGroupForDayById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceGroupForDayById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for device group by id - no id', (done) => {
        try {
          a.getAlertsForDeviceGroupForDayById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponent - errors', () => {
      it('should have a getAlertsForDeviceComponent function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for device component - no device id', (done) => {
        try {
          a.getAlertsForDeviceComponent(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for dev component - no component id', (done) => {
        try {
          a.getAlertsForDeviceComponent(deviceId, null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentById - errors', () => {
      it('should have a getAlertsForDeviceComponentById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for dev component by id - no dev id', (done) => {
        try {
          a.getAlertsForDeviceComponentById(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for dev component by id - no cmp id', (done) => {
        try {
          a.getAlertsForDeviceComponentById(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentByName - errors', () => {
      it('should have a getAlertsForDeviceComponentByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for dev component by name - no dev name', (done) => {
        try {
          a.getAlertsForDeviceComponentByName(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for dev component by name - no cmp name', (done) => {
        try {
          a.getAlertsForDeviceComponentByName(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponent - errors', () => {
      it('should have a getAlertCountsForDeviceComponent function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for dev component - no dev id', (done) => {
        try {
          a.getAlertCountsForDeviceComponent(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for dev component - no cmp id', (done) => {
        try {
          a.getAlertCountsForDeviceComponent(deviceId, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponentById - errors', () => {
      it('should have a getAlertCountsForDeviceComponentById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceComponentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for dev component by id - no dev id', (done) => {
        try {
          a.getAlertCountsForDeviceComponentById(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for dev component by id - no cmp id', (done) => {
        try {
          a.getAlertCountsForDeviceComponentById(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponentByName - errors', () => {
      it('should have a getAlertCountsForDeviceComponentByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForDeviceComponentByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for dev component by name - no dev name', (done) => {
        try {
          a.getAlertCountsForDeviceComponentByName(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for dev component by name - no cmp name', (done) => {
        try {
          a.getAlertCountsForDeviceComponentByName(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRange - errors', () => {
      it('should have a getAlertsForDeviceComponentWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for dev cmp - no dev id', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRange(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts within range for dev cmp - no cmp id', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRange(deviceId, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRangeById - errors', () => {
      it('should have a getAlertsForDeviceComponentWithRangeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentWithRangeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for dev cmp by id - no dev id', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRangeById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts within range for dev cmp by id - no cmp id', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRangeById(deviceId, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRangeByName - errors', () => {
      it('should have a getAlertsForDeviceComponentWithRangeByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentWithRangeByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts within range for dev cmp by name - no dev name', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRangeByName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts within range for dev cmp by name - no cmp name', (done) => {
        try {
          a.getAlertsForDeviceComponentWithRangeByName(deviceId, null, null, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentForDayById - errors', () => {
      it('should have a getAlertsForDeviceComponentForDayById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentForDayById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for dev cmp by id - no dev id', (done) => {
        try {
          a.getAlertsForDeviceComponentForDayById(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for day for dev cmp by id - no cmp id', (done) => {
        try {
          a.getAlertsForDeviceComponentForDayById(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentForDayByName - errors', () => {
      it('should have a getAlertsForDeviceComponentForDayByName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForDeviceComponentForDayByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for day for dev cmp by name - no dev name', (done) => {
        try {
          a.getAlertsForDeviceComponentForDayByName(null, null, (data, error) => {
            try {
              const displayE = 'device criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for day for dev cmp by name - no cmp name', (done) => {
        try {
          a.getAlertsForDeviceComponentForDayByName(deviceId, null, (data, error) => {
            try {
              const displayE = 'component criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForDeviceComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroup - errors', () => {
      it('should have a getAlertsForComponentGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForComponentGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for component group - no id', (done) => {
        try {
          a.getAlertsForComponentGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupById - errors', () => {
      it('should have a getAlertsForComponentGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForComponentGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for component group by id - no id', (done) => {
        try {
          a.getAlertsForComponentGroupById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForComponentGroup - errors', () => {
      it('should have a getAlertCountsForComponentGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForComponentGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for component group - no id', (done) => {
        try {
          a.getAlertCountsForComponentGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForComponentGroupById - errors', () => {
      it('should have a getAlertCountsForComponentGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForComponentGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for component group by id - no id', (done) => {
        try {
          a.getAlertCountsForComponentGroupById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupWithRange - errors', () => {
      it('should have a getAlertsForComponentGroupWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForComponentGroupWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert within range for cmp group - no id', (done) => {
        try {
          a.getAlertsForComponentGroupWithRange(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupWithRangeById - errors', () => {
      it('should have a getAlertsForComponentGroupWithRangeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForComponentGroupWithRangeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert within range for cmp group by id - no id', (done) => {
        try {
          a.getAlertsForComponentGroupWithRangeById(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupForDayById - errors', () => {
      it('should have a getAlertsForComponentGroupForDayById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForComponentGroupForDayById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert for day for cmp group by id - no id', (done) => {
        try {
          a.getAlertsForComponentGroupForDayById(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForComponentGroupWithRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMapConnection - errors', () => {
      it('should have a getAlertsForMapConnection function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMapConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for map connection - no map id', (done) => {
        try {
          a.getAlertsForMapConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for map connection - no map conn id', (done) => {
        try {
          a.getAlertsForMapConnection(mapId, null, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMapConnectionById - errors', () => {
      it('should have a getAlertsForMapConnectionById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMapConnectionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alerts for map connection by id - no map id', (done) => {
        try {
          a.getAlertsForMapConnectionById(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alerts for map connection by id - no map conn id', (done) => {
        try {
          a.getAlertsForMapConnectionById(mapId, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMapConnection - errors', () => {
      it('should have a getAlertCountsForMapConnection function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMapConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for map conn - no map id', (done) => {
        try {
          a.getAlertCountsForMapConnection(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for map conn - no map conn id', (done) => {
        try {
          a.getAlertCountsForMapConnection(mapId, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMapConnectionById - errors', () => {
      it('should have a getAlertCountsForMapConnectionById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMapConnectionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for map conn by id - no map id', (done) => {
        try {
          a.getAlertCountsForMapConnectionById(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for map conn by id - no map conn id', (done) => {
        try {
          a.getAlertCountsForMapConnectionById(mapId, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMapNode - errors', () => {
      it('should have a getAlertsForMapNode function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMapNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for map node - no map id', (done) => {
        try {
          a.getAlertsForMapNode(null, null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get alerts for map node - no map node id', (done) => {
        try {
          a.getAlertsForMapNode(mapId, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMapNodeById - errors', () => {
      it('should have a getAlertsForMapNodeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMapNodeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for map node by id - no map id', (done) => {
        try {
          a.getAlertsForMapNodeById(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on get alerts for map node by id - no map node id', (done) => {
        try {
          a.getAlertsForMapNodeById(mapId, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMapNode - errors', () => {
      it('should have a getAlertCountsForMapNode function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMapNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for map node - no map id', (done) => {
        try {
          a.getAlertCountsForMapNode(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for map node - no map node id', (done) => {
        try {
          a.getAlertCountsForMapNode(mapId, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMapNodeById - errors', () => {
      it('should have a getAlertCountsForMapNodeById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMapNodeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error on get alert counts for map node by id - no map id', (done) => {
        try {
          a.getAlertCountsForMapNodeById(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('error on get alert counts for map node by id - no map node id', (done) => {
        try {
          a.getAlertCountsForMapNodeById(mapId, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMapNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMap - errors', () => {
      it('should have a getAlertsForMap function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for map - no id', (done) => {
        try {
          a.getAlertsForMap(null, null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMapById - errors', () => {
      it('should have a getAlertsForMapById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMapById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alerts for map by id - no id', (done) => {
        try {
          a.getAlertsForMapById(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMap - errors', () => {
      it('should have a getAlertCountsForMap function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for map node - no id', (done) => {
        try {
          a.getAlertCountsForMap(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForMapById - errors', () => {
      it('should have a getAlertCountsForMapById function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertCountsForMapById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on get alert counts for map node by id - no id', (done) => {
        try {
          a.getAlertCountsForMapById(null, (data, error) => {
            try {
              const displayE = 'mapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertsForMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertId = 1;

    describe('#createAlert - errors', () => {
      it('should have a createAlert function', (done) => {
        try {
          assert.equal(true, typeof a.createAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on create an alert - no alert', (done) => {
        try {
          a.createAlert(null, (data, error) => {
            try {
              const displayE = 'alertObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create an alert - no message', (done) => {
        try {
          const alert = { origin: 'string', deviceId };
          a.createAlert(alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'message\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create an alert - no origin', (done) => {
        try {
          const alert = { message: 'blah', deviceId };
          a.createAlert(alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'origin\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on create an alert - no device/object', (done) => {
        try {
          const alert = { message: 'blah', origin: 'string' };
          a.createAlert(alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'deviceId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlert - errors', () => {
      it('should have a updateAlert function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on update an alert - no id', (done) => {
        try {
          a.updateAlert(null, null, (data, error) => {
            try {
              const displayE = 'alertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update an alert - no changes', (done) => {
        try {
          a.updateAlert(alertId, null, (data, error) => {
            try {
              const displayE = 'alertObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update an alert - no message', (done) => {
        try {
          const alert = { origin: 'string', deviceId };
          a.updateAlert(alertId, alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'message\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update an alert - no origin', (done) => {
        try {
          const alert = { message: 'blah', deviceId };
          a.updateAlert(alertId, alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'origin\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on update an alert - no device/object', (done) => {
        try {
          const alert = { message: 'blah', origin: 'string' };
          a.updateAlert(alertId, alert, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'deviceId\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-sevone-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignAlert - errors', () => {
      it('should have a assignAlert function', (done) => {
        try {
          assert.equal(true, typeof a.assignAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on assign an alert - no id', (done) => {
        try {
          a.assignAlert(null, null, (data, error) => {
            try {
              const displayE = 'alertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-assignAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on assign an alert - no username', (done) => {
        try {
          a.assignAlert(alertId, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-assignAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ignoreAlert - errors', () => {
      it('should have a ignoreAlert function', (done) => {
        try {
          assert.equal(true, typeof a.ignoreAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on ignore an alert - no id', (done) => {
        try {
          a.ignoreAlert(null, null, null, (data, error) => {
            try {
              const displayE = 'alertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-ignoreAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on ignore an alert - no message', (done) => {
        try {
          a.ignoreAlert(alertId, null, null, (data, error) => {
            try {
              const displayE = 'message is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-ignoreAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on ignore an alert - no ignore until time', (done) => {
        try {
          a.ignoreAlert(alertId, 'because', null, (data, error) => {
            try {
              const displayE = 'ignoreUntil is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-ignoreAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearAlert - errors', () => {
      it('should have a clearAlert function', (done) => {
        try {
          assert.equal(true, typeof a.clearAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on clear an alert - no id', (done) => {
        try {
          a.clearAlert(null, null, (data, error) => {
            try {
              const displayE = 'alertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-clearAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error on clear an alert - no message', (done) => {
        try {
          a.clearAlert(alertId, null, (data, error) => {
            try {
              const displayE = 'message is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-clearAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlert - errors', () => {
      it('should have a deleteAlert function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should error on delete an alert - no id', (done) => {
        try {
          a.deleteAlert(null, (data, error) => {
            try {
              const displayE = 'alertId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentResources - errors', () => {
      it('should have a getObjectAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectAttachmentResources - errors', () => {
      it('should have a updateObjectAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentSettings - errors', () => {
      it('should have a getObjectAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectAttachmentSettings - errors', () => {
      it('should have a updateObjectAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentVisualizationSettings - errors', () => {
      it('should have a getObjectAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectAttachmentVisualizationSettings - errors', () => {
      it('should have a updateObjectAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateObjectAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateObjectAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateObjectAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateObjectAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateObjectAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createObjectAttachment - errors', () => {
      it('should have a createObjectAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createObjectAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createObjectAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createObjectAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createObjectAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createObjectAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypes - errors', () => {
      it('should have a getDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceType - errors', () => {
      it('should have a createDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceType(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeForDeviceById - errors', () => {
      it('should have a getDeviceTypeForDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceTypeForDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceTypeForDeviceById(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceTypeForDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeById - errors', () => {
      it('should have a getDeviceTypeById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceTypeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceTypeById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceTypeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeById - errors', () => {
      it('should have a deleteDeviceTypeById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceTypeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDeviceTypeById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceTypeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMemberByIdToType - errors', () => {
      it('should have a addMemberByIdToType function', (done) => {
        try {
          assert.equal(true, typeof a.addMemberByIdToType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.addMemberByIdToType(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addMemberByIdToType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.addMemberByIdToType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-addMemberByIdToType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeMemberById - errors', () => {
      it('should have a deleteDeviceTypeMemberById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceTypeMemberById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDeviceTypeMemberById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceTypeMemberById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.deleteDeviceTypeMemberById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceTypeMemberById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateObjectById - errors', () => {
      it('should have a partiallyUpdateObjectById function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateObjectById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.partiallyUpdateObjectById(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateObjectById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateObjectById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMapAttachmentResources - errors', () => {
      it('should have a getStatusMapAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getStatusMapAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getStatusMapAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getStatusMapAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStatusMapAttachmentResources - errors', () => {
      it('should have a updateStatusMapAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateStatusMapAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateStatusMapAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateStatusMapAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateStatusMapAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateStatusMapAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStatusMapAttachment - errors', () => {
      it('should have a createStatusMapAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createStatusMapAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createStatusMapAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createStatusMapAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStatusMapAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createStatusMapAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeys - errors', () => {
      it('should have a getApiKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getApiKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApiKey - errors', () => {
      it('should have a createApiKey function', (done) => {
        try {
          assert.equal(true, typeof a.createApiKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApiKey(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeys - errors', () => {
      it('should have a deleteApiKeys function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApiKey - errors', () => {
      it('should have a updateApiKey function', (done) => {
        try {
          assert.equal(true, typeof a.updateApiKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateApiKey(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApiKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKey - errors', () => {
      it('should have a deleteApiKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteApiKey(null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeysForUser - errors', () => {
      it('should have a getApiKeysForUser function', (done) => {
        try {
          assert.equal(true, typeof a.getApiKeysForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getApiKeysForUser(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getApiKeysForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApiKeyForUser - errors', () => {
      it('should have a createApiKeyForUser function', (done) => {
        try {
          assert.equal(true, typeof a.createApiKeyForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createApiKeyForUser(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApiKeyForUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeysForUser - errors', () => {
      it('should have a deleteApiKeysForUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiKeysForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApiKeysForUser(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteApiKeysForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApiKeyForUser - errors', () => {
      it('should have a updateApiKeyForUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateApiKeyForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateApiKeyForUser(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.updateApiKeyForUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApiKeyForUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeyForUser - errors', () => {
      it('should have a deleteApiKeyForUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiKeyForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiKey', (done) => {
        try {
          a.deleteApiKeyForUser(null, null, (data, error) => {
            try {
              const displayE = 'apiKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApiKeyForUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteApiKeyForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesInDiscovery - errors', () => {
      it('should have a getDevicesInDiscovery function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesInDiscovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterDevicesInDiscovery - errors', () => {
      it('should have a filterDevicesInDiscovery function', (done) => {
        try {
          assert.equal(true, typeof a.filterDevicesInDiscovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.filterDevicesInDiscovery('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-filterDevicesInDiscovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceStatusById - errors', () => {
      it('should have a getDeviceStatusById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceStatusById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceStatusById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceStatusById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevicePriority - errors', () => {
      it('should have a updateDevicePriority function', (done) => {
        try {
          assert.equal(true, typeof a.updateDevicePriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDevicePriority(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDevicePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDevicePriority('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDevicePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilterSchema - errors', () => {
      it('should have a getFlowFalconAttachmentFilterSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentFilterSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilters - errors', () => {
      it('should have a getFlowFalconAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconAttachmentFilters(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlowFalconAttachmentFilters - errors', () => {
      it('should have a updateFlowFalconAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlowFalconAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFlowFalconAttachmentFilters(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFlowFalconAttachmentFilters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentResources - errors', () => {
      it('should have a getFlowFalconAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlowFalconAttachmentResources - errors', () => {
      it('should have a updateFlowFalconAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlowFalconAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFlowFalconAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFlowFalconAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentSettings - errors', () => {
      it('should have a getFlowFalconAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlowFalconAttachmentSettings - errors', () => {
      it('should have a updateFlowFalconAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlowFalconAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFlowFalconAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFlowFalconAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateFlowFalconAttachmentSettings - errors', () => {
      it('should have a partiallyUpdateFlowFalconAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateFlowFalconAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateFlowFalconAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateFlowFalconAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentTimeSettings - errors', () => {
      it('should have a getFlowFalconAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlowFalconAttachmentTimeSettings - errors', () => {
      it('should have a updateFlowFalconAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlowFalconAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFlowFalconAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFlowFalconAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should have a getFlowFalconAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should have a updateFlowFalconAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlowFalconAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFlowFalconAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFlowFalconAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateFlowFalconAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateFlowFalconAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateFlowFalconAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateFlowFalconAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateFlowFalconAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFlowFalconAttachment - errors', () => {
      it('should have a createFlowFalconAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createFlowFalconAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createFlowFalconAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createFlowFalconAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFlowFalconAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createFlowFalconAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentResources - errors', () => {
      it('should have a getTopNAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getTopNAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTopNAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTopNAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTopNAttachmentResources - errors', () => {
      it('should have a updateTopNAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateTopNAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTopNAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTopNAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentSettings - errors', () => {
      it('should have a getTopNAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTopNAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTopNAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTopNAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTopNAttachmentSettings - errors', () => {
      it('should have a updateTopNAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTopNAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTopNAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTopNAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateTopNAttachmentSettings - errors', () => {
      it('should have a partiallyUpdateTopNAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateTopNAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateTopNAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateTopNAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentTimeSettings - errors', () => {
      it('should have a getTopNAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTopNAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTopNAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTopNAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTopNAttachmentTimeSettings - errors', () => {
      it('should have a updateTopNAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTopNAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTopNAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTopNAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentVisualizationSettings - errors', () => {
      it('should have a getTopNAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTopNAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTopNAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTopNAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTopNAttachmentVisualizationSettings - errors', () => {
      it('should have a updateTopNAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTopNAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTopNAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTopNAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTopNAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateTopNAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateTopNAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateTopNAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateTopNAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateTopNAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTopNAttachment - errors', () => {
      it('should have a createTopNAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createTopNAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createTopNAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createTopNAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTopNAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createTopNAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeersUsingGET - errors', () => {
      it('should have a getPeersUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPeersUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterSettings - errors', () => {
      it('should have a getClusterSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentPeerUsingGET - errors', () => {
      it('should have a getCurrentPeerUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentPeerUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncorporateModeUsingGET - errors', () => {
      it('should have a getIncorporateModeUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIncorporateModeUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIncorporateModeUsingPATCH - errors', () => {
      it('should have a editIncorporateModeUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.editIncorporateModeUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIncorporateModeUsingPATCH(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-editIncorporateModeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeerUsingGET - errors', () => {
      it('should have a getPeerUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPeerUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPeerUsingGET(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPeerUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should have a getSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportAttachment - errors', () => {
      it('should have a getReportAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getReportAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getReportAttachment(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getReportAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReportAttachmentById - errors', () => {
      it('should have a updateReportAttachmentById function', (done) => {
        try {
          assert.equal(true, typeof a.updateReportAttachmentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateReportAttachmentById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportAttachmentById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateReportAttachmentById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportAttachmentById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportAttachment - errors', () => {
      it('should have a deleteReportAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReportAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteReportAttachment(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteReportAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportAttachmentEndpoints - errors', () => {
      it('should have a getAllReportAttachmentEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getAllReportAttachmentEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAllReportAttachmentEndpoints(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAllReportAttachmentEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportAttachments - errors', () => {
      it('should have a getAllReportAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getAllReportAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAllReportAttachments(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAllReportAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupsRules - errors', () => {
      it('should have a getObjectGroupsRules function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectGroupsRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createObjectGroupRule - errors', () => {
      it('should have a createObjectGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.createObjectGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createObjectGroupRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createObjectGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRule - errors', () => {
      it('should have a getObjectGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectGroupRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectGroupRule - errors', () => {
      it('should have a updateObjectGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectGroupRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectGroupRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectGroupRule - errors', () => {
      it('should have a deleteObjectGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteObjectGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteObjectGroupRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteObjectGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRules - errors', () => {
      it('should have a getObjectGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectGroupRules(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectGroupRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyObjectGroupRules - errors', () => {
      it('should have a applyObjectGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.applyObjectGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.applyObjectGroupRules(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-applyObjectGroupRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentUser - errors', () => {
      it('should have a getCurrentUser function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentResources - errors', () => {
      it('should have a getObjectGroupAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectGroupAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectGroupAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectGroupAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectGroupAttachmentResources - errors', () => {
      it('should have a updateObjectGroupAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectGroupAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectGroupAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectGroupAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should have a getObjectGroupAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectGroupAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getObjectGroupAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectGroupAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should have a updateObjectGroupAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateObjectGroupAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateObjectGroupAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObjectGroupAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateObjectGroupAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateObjectGroupAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateObjectGroupAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateObjectGroupAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectGroupAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateObjectGroupAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectGroupAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createObjectGroupAttachment - errors', () => {
      it('should have a createObjectGroupAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createObjectGroupAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createObjectGroupAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createObjectGroupAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createObjectGroupAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createObjectGroupAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateStatusMapById - errors', () => {
      it('should have a partiallyUpdateStatusMapById function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateStatusMapById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateStatusMapById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateStatusMapById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateStatusMapById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateStatusMapById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilterSchema - errors', () => {
      it('should have a getDeviceAttachmentFilterSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAttachmentFilterSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilters - errors', () => {
      it('should have a getDeviceAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceAttachmentFilters(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceAttachmentFilters - errors', () => {
      it('should have a updateDeviceAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceAttachmentFilters(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceAttachmentFilters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachment - errors', () => {
      it('should have a getDeviceAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceAttachment(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceAttachment - errors', () => {
      it('should have a updateDeviceAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentSettings - errors', () => {
      it('should have a getDeviceAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceAttachmentSettings - errors', () => {
      it('should have a updateDeviceAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentVisualization - errors', () => {
      it('should have a getDeviceAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceAttachmentVisualization(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceAttachmentVisualization - errors', () => {
      it('should have a updateDeviceAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceAttachmentVisualization(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceAttachmentVisualization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceAttachment - errors', () => {
      it('should have a createDeviceAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createDeviceAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachment - errors', () => {
      it('should have a getDeviceGroupsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceGroupsAttachment(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceGroupsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroupsAttachment - errors', () => {
      it('should have a updateDeviceGroupsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroupsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceGroupsAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceGroupsAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachmentVisualization - errors', () => {
      it('should have a getDeviceGroupsAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupsAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceGroupsAttachmentVisualization(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceGroupsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroupsAttachmentVisualization - errors', () => {
      it('should have a updateDeviceGroupsAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroupsAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceGroupsAttachmentVisualization(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceGroupsAttachmentVisualization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceGroupsAttachment - errors', () => {
      it('should have a createDeviceGroupsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceGroupsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createDeviceGroupsAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceGroupsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceGroupsAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceGroupsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentResources - errors', () => {
      it('should have a getMetadataAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getMetadataAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getMetadataAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMetadataAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMetadataAttachmentResources - errors', () => {
      it('should have a updateMetadataAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updateMetadataAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateMetadataAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMetadataAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMetadataAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMetadataAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentVisualizationSettings - errors', () => {
      it('should have a getMetadataAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getMetadataAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getMetadataAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMetadataAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should have a updateMetadataAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateMetadataAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateMetadataAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMetadataAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMetadataAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateMetadataAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateMetadataAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateMetadataAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateMetadataAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateMetadataAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateMetadataAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateMetadataAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMetadataAttachment - errors', () => {
      it('should have a createMetadataAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createMetadataAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createMetadataAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMetadataAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMetadataAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createMetadataAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentResources - errors', () => {
      it('should have a getPerformanceMetricsAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getPerformanceMetricsAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPerformanceMetricsAttachmentResources(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPerformanceMetricsAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePerformanceMetricsAttachmentResources - errors', () => {
      it('should have a updatePerformanceMetricsAttachmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.updatePerformanceMetricsAttachmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentResources(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentSettings - errors', () => {
      it('should have a getPerformanceMetricsAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getPerformanceMetricsAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPerformanceMetricsAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPerformanceMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should have a updatePerformanceMetricsAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updatePerformanceMetricsAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should have a partiallyUpdatePerformanceMetricsAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdatePerformanceMetricsAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdatePerformanceMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdatePerformanceMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should have a getPerformanceMetricsAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getPerformanceMetricsAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPerformanceMetricsAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPerformanceMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should have a updatePerformanceMetricsAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updatePerformanceMetricsAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should have a getPerformanceMetricsAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getPerformanceMetricsAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPerformanceMetricsAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPerformanceMetricsAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should have a updatePerformanceMetricsAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updatePerformanceMetricsAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePerformanceMetricsAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPerformanceMetricsAttachment - errors', () => {
      it('should have a createPerformanceMetricsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createPerformanceMetricsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createPerformanceMetricsAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPerformanceMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPerformanceMetricsAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPerformanceMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachment - errors', () => {
      it('should have a getGroupMetricsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupMetricsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getGroupMetricsAttachment(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getGroupMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupMetricsAttachment - errors', () => {
      it('should have a updateGroupMetricsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupMetricsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateGroupMetricsAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGroupMetricsAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentSettings - errors', () => {
      it('should have a getGroupMetricsAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupMetricsAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getGroupMetricsAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getGroupMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupMetricsAttachmentSettings - errors', () => {
      it('should have a updateGroupMetricsAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupMetricsAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateGroupMetricsAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGroupMetricsAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should have a getGroupMetricsAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupMetricsAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getGroupMetricsAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getGroupMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should have a updateGroupMetricsAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupMetricsAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateGroupMetricsAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGroupMetricsAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentVisualization - errors', () => {
      it('should have a getGroupMetricsAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupMetricsAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getGroupMetricsAttachmentVisualization(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getGroupMetricsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupMetricsAttachmentVisualization - errors', () => {
      it('should have a updateGroupMetricsAttachmentVisualization function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupMetricsAttachmentVisualization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateGroupMetricsAttachmentVisualization(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGroupMetricsAttachmentVisualization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateGroupMetricsAttachmentVisualization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupMetricsAttachment - errors', () => {
      it('should have a createGroupMetricsAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupMetricsAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createGroupMetricsAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createGroupMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGroupMetricsAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createGroupMetricsAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSessions - errors', () => {
      it('should have a getActiveSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keepAlive - errors', () => {
      it('should have a keepAlive function', (done) => {
        try {
          assert.equal(true, typeof a.keepAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signIn - errors', () => {
      it('should have a signIn function', (done) => {
        try {
          assert.equal(true, typeof a.signIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.signIn('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-signIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOut - errors', () => {
      it('should have a signOut function', (done) => {
        try {
          assert.equal(true, typeof a.signOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOutOthers - errors', () => {
      it('should have a signOutOthers function', (done) => {
        try {
          assert.equal(true, typeof a.signOutOthers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOutUser - errors', () => {
      it('should have a signOutUser function', (done) => {
        try {
          assert.equal(true, typeof a.signOutUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.signOutUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-signOutUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateDeviceGroupById - errors', () => {
      it('should have a partiallyUpdateDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateDeviceGroupById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateDeviceGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTagsById - errors', () => {
      it('should have a getDeviceTagsById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceTagsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceTagsById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceTagsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilterSchema - errors', () => {
      it('should have a getAlertAttachmentFilterSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentFilterSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentAggregation - errors', () => {
      it('should have a getAlertAttachmentAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentAggregation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentAggregation - errors', () => {
      it('should have a updateAlertAttachmentAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentAggregation(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentAggregation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilters - errors', () => {
      it('should have a getAlertAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentFilters(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentFilters - errors', () => {
      it('should have a updateAlertAttachmentFilters function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentFilters(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentFilters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentResource - errors', () => {
      it('should have a getAlertAttachmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentResource - errors', () => {
      it('should have a updateAlertAttachmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentSettings - errors', () => {
      it('should have a getAlertAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentSettings - errors', () => {
      it('should have a updateAlertAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentTimeSettings - errors', () => {
      it('should have a getAlertAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentTimeSettings - errors', () => {
      it('should have a updateAlertAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentVisualizationSettings - errors', () => {
      it('should have a getAlertAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAlertAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getAlertAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlertAttachmentVisualizationSettings - errors', () => {
      it('should have a updateAlertAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlertAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAlertAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlertAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateAlertAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateAlertAttachmentVisualizationSettings - errors', () => {
      it('should have a partiallyUpdateAlertAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateAlertAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateAlertAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateAlertAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateAlertAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateAlertAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlertAttachment - errors', () => {
      it('should have a createAlertAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createAlertAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createAlertAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createAlertAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAlertAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createAlertAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPlugins - errors', () => {
      it('should have a getAllPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicePluginInfoSchema - errors', () => {
      it('should have a getDevicePluginInfoSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicePluginInfoSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorExtendedInfoSchema - errors', () => {
      it('should have a getIndicatorExtendedInfoSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getIndicatorExtendedInfoSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginId', (done) => {
        try {
          a.getIndicatorExtendedInfoSchema(null, (data, error) => {
            try {
              const displayE = 'pluginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getIndicatorExtendedInfoSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginIndicatorTypes - errors', () => {
      it('should have a getAllPluginIndicatorTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPluginIndicatorTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPluginIndicatorType - errors', () => {
      it('should have a createPluginIndicatorType function', (done) => {
        try {
          assert.equal(true, typeof a.createPluginIndicatorType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPluginIndicatorType(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPluginIndicatorType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterPluginIndicatorTypes - errors', () => {
      it('should have a filterPluginIndicatorTypes function', (done) => {
        try {
          assert.equal(true, typeof a.filterPluginIndicatorTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginIndicatorTypes - errors', () => {
      it('should have a getSchemaForAllPluginIndicatorTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getSchemaForAllPluginIndicatorTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginId', (done) => {
        try {
          a.getSchemaForAllPluginIndicatorTypes(null, (data, error) => {
            try {
              const displayE = 'pluginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getSchemaForAllPluginIndicatorTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePluginIndicatorType - errors', () => {
      it('should have a updatePluginIndicatorType function', (done) => {
        try {
          assert.equal(true, typeof a.updatePluginIndicatorType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePluginIndicatorType(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePluginIndicatorType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePluginIndicatorType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePluginIndicatorType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectExtendedInfoSchema - errors', () => {
      it('should have a getObjectExtendedInfoSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectExtendedInfoSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginId', (done) => {
        try {
          a.getObjectExtendedInfoSchema(null, (data, error) => {
            try {
              const displayE = 'pluginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getObjectExtendedInfoSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginObjectTypes - errors', () => {
      it('should have a getAllPluginObjectTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPluginObjectTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPluginObjectType - errors', () => {
      it('should have a createPluginObjectType function', (done) => {
        try {
          assert.equal(true, typeof a.createPluginObjectType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPluginObjectType(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPluginObjectType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterPluginObjectTypes - errors', () => {
      it('should have a filterPluginObjectTypes function', (done) => {
        try {
          assert.equal(true, typeof a.filterPluginObjectTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginObjectTypes - errors', () => {
      it('should have a getSchemaForAllPluginObjectTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getSchemaForAllPluginObjectTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginId', (done) => {
        try {
          a.getSchemaForAllPluginObjectTypes(null, (data, error) => {
            try {
              const displayE = 'pluginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getSchemaForAllPluginObjectTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePluginObjectType - errors', () => {
      it('should have a updatePluginObjectType function', (done) => {
        try {
          assert.equal(true, typeof a.updatePluginObjectType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePluginObjectType(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePluginObjectType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePluginObjectType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePluginObjectType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlertForced - errors', () => {
      it('should have a createAlertForced function', (done) => {
        try {
          assert.equal(true, typeof a.createAlertForced === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAlertForced(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createAlertForced', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconDeviceAlerts - errors', () => {
      it('should have a getFlowFalconDeviceAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowFalconDeviceAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFlowFalconDeviceAlerts(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getFlowFalconDeviceAlerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaxSeverityAlertForObjects - errors', () => {
      it('should have a getMaxSeverityAlertForObjects function', (done) => {
        try {
          assert.equal(true, typeof a.getMaxSeverityAlertForObjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getMaxSeverityAlertForObjects(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMaxSeverityAlertForObjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAlert - errors', () => {
      it('should have a patchAlert function', (done) => {
        try {
          assert.equal(true, typeof a.patchAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchAlert(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-patchAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAlert('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-patchAlert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimezonesByCountries - errors', () => {
      it('should have a getTimezonesByCountries function', (done) => {
        try {
          assert.equal(true, typeof a.getTimezonesByCountries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should have a getPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicy - errors', () => {
      it('should have a createPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterPolicies - errors', () => {
      it('should have a filterPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.filterPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.filterPolicies('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-filterPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyFolders - errors', () => {
      it('should have a getPolicyFolders function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyFolders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyFolder - errors', () => {
      it('should have a createPolicyFolder function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicyFolder(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPolicyFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyFolder - errors', () => {
      it('should have a updatePolicyFolder function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePolicyFolder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyFolder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyFolderById - errors', () => {
      it('should have a deletePolicyFolderById function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyFolderById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicyFolderById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deletePolicyFolderById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should have a getPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicy - errors', () => {
      it('should have a updatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePolicy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyById - errors', () => {
      it('should have a deletePolicyById function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicyById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deletePolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActionsUsingGET - errors', () => {
      it('should have a getActionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getActionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getActionsUsingGET(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getActionsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyAction - errors', () => {
      it('should have a updatePolicyAction function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePolicyAction(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyAction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyActionById - errors', () => {
      it('should have a deletePolicyActionById function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyActionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePolicyActionById(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deletePolicyActionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAllUsingGET - errors', () => {
      it('should have a findAllUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.findAllUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.findAllUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-findAllUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyCondition - errors', () => {
      it('should have a createPolicyCondition function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyCondition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createPolicyCondition(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPolicyCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicyCondition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createPolicyCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyConditionById - errors', () => {
      it('should have a getPolicyConditionById function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyConditionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getPolicyConditionById(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPolicyConditionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conditionId', (done) => {
        try {
          a.getPolicyConditionById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'conditionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getPolicyConditionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyCondition - errors', () => {
      it('should have a updatePolicyCondition function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyCondition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePolicyCondition(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conditionId', (done) => {
        try {
          a.updatePolicyCondition('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'conditionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyCondition('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updatePolicyCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyConditionById - errors', () => {
      it('should have a deletePolicyConditionById function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyConditionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePolicyConditionById(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deletePolicyConditionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conditionId', (done) => {
        try {
          a.deletePolicyConditionById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'conditionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deletePolicyConditionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReports - errors', () => {
      it('should have a getAllReports function', (done) => {
        try {
          assert.equal(true, typeof a.getAllReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createReport - errors', () => {
      it('should have a createReport function', (done) => {
        try {
          assert.equal(true, typeof a.createReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createReport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportFolders - errors', () => {
      it('should have a getAllReportFolders function', (done) => {
        try {
          assert.equal(true, typeof a.getAllReportFolders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createReportFolder - errors', () => {
      it('should have a createReportFolder function', (done) => {
        try {
          assert.equal(true, typeof a.createReportFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createReportFolder(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createReportFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReportFolderById - errors', () => {
      it('should have a updateReportFolderById function', (done) => {
        try {
          assert.equal(true, typeof a.updateReportFolderById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateReportFolderById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportFolderById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateReportFolderById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportFolderById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportFolderById - errors', () => {
      it('should have a deleteReportFolderById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReportFolderById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteReportFolderById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteReportFolderById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReport - errors', () => {
      it('should have a getReport function', (done) => {
        try {
          assert.equal(true, typeof a.getReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getReport(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReportById - errors', () => {
      it('should have a updateReportById function', (done) => {
        try {
          assert.equal(true, typeof a.updateReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateReportById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateReportById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportById - errors', () => {
      it('should have a deleteReportById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteReportById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentAggregation - errors', () => {
      it('should have a getTelephonyAttachmentAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.getTelephonyAttachmentAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTelephonyAttachmentAggregation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTelephonyAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTelephonyAttachmentAggregation - errors', () => {
      it('should have a updateTelephonyAttachmentAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.updateTelephonyAttachmentAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTelephonyAttachmentAggregation(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTelephonyAttachmentAggregation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentSettings - errors', () => {
      it('should have a getTelephonyAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTelephonyAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTelephonyAttachmentSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTelephonyAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTelephonyAttachmentSettings - errors', () => {
      it('should have a updateTelephonyAttachmentSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTelephonyAttachmentSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTelephonyAttachmentSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTelephonyAttachmentSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentTimeSettings - errors', () => {
      it('should have a getTelephonyAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTelephonyAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTelephonyAttachmentTimeSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTelephonyAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTelephonyAttachmentTimeSettings - errors', () => {
      it('should have a updateTelephonyAttachmentTimeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTelephonyAttachmentTimeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTelephonyAttachmentTimeSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTelephonyAttachmentTimeSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentTimeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should have a getTelephonyAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTelephonyAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTelephonyAttachmentVisualizationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getTelephonyAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should have a updateTelephonyAttachmentVisualizationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTelephonyAttachmentVisualizationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTelephonyAttachmentVisualizationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTelephonyAttachmentVisualizationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateTelephonyAttachmentVisualizationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTelephonyAttachment - errors', () => {
      it('should have a createTelephonyAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createTelephonyAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createTelephonyAttachment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createTelephonyAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTelephonyAttachment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createTelephonyAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDynamicPluginManager - errors', () => {
      it('should have a registerDynamicPluginManager function', (done) => {
        try {
          assert.equal(true, typeof a.registerDynamicPluginManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.registerDynamicPluginManager(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-registerDynamicPluginManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDynamicPlugin - errors', () => {
      it('should have a registerDynamicPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.registerDynamicPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.registerDynamicPlugin(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-registerDynamicPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.registerDynamicPlugin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-registerDynamicPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateObjectGroupById - errors', () => {
      it('should have a partiallyUpdateObjectGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateObjectGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateObjectGroupById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateObjectGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateObjectGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partiallyUpdateDeviceById - errors', () => {
      it('should have a partiallyUpdateDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.partiallyUpdateDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.partiallyUpdateDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.partiallyUpdateDeviceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-partiallyUpdateDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapImages - errors', () => {
      it('should have a getMapImages function', (done) => {
        try {
          assert.equal(true, typeof a.getMapImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapImageById - errors', () => {
      it('should have a getMapImageById function', (done) => {
        try {
          assert.equal(true, typeof a.getMapImageById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getMapImageById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getMapImageById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoggingLevel - errors', () => {
      it('should have a postLoggingLevel function', (done) => {
        try {
          assert.equal(true, typeof a.postLoggingLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postLoggingLevel(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-postLoggingLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicMetrics - errors', () => {
      it('should have a getPublicMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getPublicMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should have a getVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsRules - errors', () => {
      it('should have a getDeviceGroupsRules function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupsRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceGroupRule - errors', () => {
      it('should have a createDeviceGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceGroupRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-createDeviceGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRule - errors', () => {
      it('should have a getDeviceGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceGroupRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroupRule - errors', () => {
      it('should have a updateDeviceGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDeviceGroupRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceGroupRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-updateDeviceGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupRule - errors', () => {
      it('should have a deleteDeviceGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDeviceGroupRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-deleteDeviceGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRules - errors', () => {
      it('should have a getDeviceGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDeviceGroupRules(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-sevone-adapter-getDeviceGroupRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
