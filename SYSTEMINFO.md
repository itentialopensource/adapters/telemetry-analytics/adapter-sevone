# SevOne

Vendor: IBM
Homepage: https://www.ibm.com/us-en

Product: SevOne
Product Page: https://www.ibm.com/products/sevone-network-performance-management

## Introduction
We classify SevOne into the Service Assurance domain as SevOne provides the capabilities to monitor and manage the performance and availability of the IT services.

"SevOne delivers a comprehensive view of what’s happening in the network and how that performance affects the applications driving modern businesses"

## Why Integrate
The SevOne adapter from Itential is used to integrate the Itential Automation Platform (IAP) with IBM SevOne Network Performance Management to monitor network and infrastructure to maintain the reliability and quality of the services. With this adapter you have the ability to perform operations such as:

- Retrieve, Create, or Clear Alerts
- Retrieve, Create, or Update Policies
- Get, Create, Update, or Delete Device
- Get, Create, Update, or Delete Device Groups

## Additional Product Documentation
The [API documents for SevOne](https://developer.ibm.com/apis/catalog/sevonenpm--sevone-api/Introduction)