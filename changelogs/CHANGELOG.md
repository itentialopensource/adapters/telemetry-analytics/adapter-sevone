
## 2.4.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!12

---

## 2.3.1 [06-30-2021]

- Fixes for number formats not supported by IAP (remove int64 and int32)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!11

---

## 2.3.0 [03-29-2021]

- Received a more full swagger so need to add the v1 calls that were missing to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!10

---

## 2.2.4 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!9

---

## 2.2.3 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!8

---

## 2.2.2 [02-19-2020]

- Updated the syntax error on base_path.

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!7

---

## 2.2.1 [01-16-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!6

---

## 2.2.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!5

---

## 2.1.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!4

---
## 2.0.7 [07-30-2019] & 2.0.6 [07-16-2019] & 2.0.5 [07-16-2019]

- Updates for artifacts

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!3

---

## 2.0.4 [07-15-2019]

- Migrated to the latest version, added support for app-artifact and categorized the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone!2

---

## 2.0.2 [04-09-2019] & 2.0.1 [04-09-2019]

- Converts the SevOne adapter to an open source adapter.

See merge request itentialopensource/adapters/adapter-sevone!1

---
